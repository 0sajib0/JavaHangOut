package job;

public class DefaultDatatypeValue {
	public static void main(String arg[]){
		int it[]=new int[10];
		String st[]=new String[10];
		double d[] = new double[10];
		boolean bl[]=new boolean[10];
		char c[]=new  char[20];
		
		System.out.println("int = "+it[0]);
		System.out.println("string = "+st[0]);
		System.out.println("double = "+d[0]);
		System.out.println("boolean = "+bl[0]);
		System.out.println("char = "+c[0]);
	}

}
